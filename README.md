## Android Commandline

`Eclipse` veya `Android Studio` gibi bir IDE kullanmadan ve sadece komut satırı komutları kullanılarak bir Android uygulaması nasıl oluşturulur? Bu kendi yazdığınız bir Build dosyası ve Build adımlarını kendiniz işletmeniz ile mümkündür. Cok pratik olduğu için burada `Ant` Build dosyası kullanıyoruz.


Wie erstellt man eine Android-Anwendung ohne eine IDE wie `Eclipse` oder `Android Studio` zu benutzen und indem man nur die Kommandozeilebefehlen verwenden? Dies ist durch eine selbst geschriebene Builddatei und dadurch man die Build-Schritte selbst durchführt, möglich. Hier verwenden wir zur Erstellung der Projekt eine `Ant`-Builddatei weil sie echt praktisch ist.

![Demo Video](Bildschirm-Aufnahme-android-cl.mp4)

Komut satırı komutları ile oluşturulan bir Android uygulaması
Eine Android-Anwendung die mittels der Kommandozeilebefehlen erstellt wurde.

![Bildschirm](android-commandline-anwendung.png)

Kaynaklar:
Quelle:

1- Beginning Android 4 - Grant Allen

2- http://www.androidengineer.com/2010/06/using-ant-to-automate-building-android.html
